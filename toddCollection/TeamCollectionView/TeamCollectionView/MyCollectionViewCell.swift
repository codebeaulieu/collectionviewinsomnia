//
//  MyCollectionViewCell.swift
//  TeamCollectionView
//
//  Created by Todd Kinsman on 1/25/16.
//  Copyright © 2016 Todd Kinsman. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myLabel: UILabel!
    
    //hightlights cell when you click and hold
    override var highlighted: Bool {
        didSet {
            if (highlighted) {
                self.layer.opacity = 0.6;
            } else {
                self.layer.opacity = 1.0;
            }
        }
    }
    
}
